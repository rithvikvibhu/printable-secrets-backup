'''
File name: psb.py
Author: Rithvik Vibhu
Python version: 3.x

This script is used to parse a json file of secrets and generate a printable document based on a template.
Requires: all-codes.json, print-template.docx
'''
import json
from datetime import datetime
from docxtpl import DocxTemplate, RichText

JSON_FILENAME = "secrets.json"
TEMPLATE_FILENAME = "print_template.docx"
OUTPUT_FILENAME = "generated_doc.docx"

# Read json file
with open(JSON_FILENAME, "r") as f:
    data = json.load(f)

# Add special items
special_content = RichText()
for item in data["specials"]:
    special_content.add(item + ':\n', style='smallcode2')
    special_content.add(data["specials"][item], style='smallcode2')

# Generate document
doc = DocxTemplate(TEMPLATE_FILENAME)
context = { 'date': datetime.now().strftime("%a, %d %b %Y %H:%M:%S %Z"), 'backup_codes' : data["backup-codes"], 'totp_secrets': data["totp-secrets"], 'special_content': special_content }
doc.render(context, autoescape=True)
doc.save(OUTPUT_FILENAME)

print("Done! You can now print {}.".format(OUTPUT_FILENAME))