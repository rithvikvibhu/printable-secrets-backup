# printable-secrets-backup

This project provides a standard template to store secrets (as json) and to generate a print-friendly document from the same file.

**Note:**
>None of the fields are mandatory. Save what you like here.  
>**Also, it is up to you to encrypt the file and to keep it safe**

**sample_secrets.json** has 3 main parts:

- **backup-codes**
    - This is for storing backup codes for services using 2FA. Can also be used for non-2FA recovery codes
    - Each service has an array of code objects with 2 keys - *code* (string) and *used* (boolean).

- **totp-secrets**
    - This is for storing 2fa totp seeds.
    - Each website has 2 important fields: *username* (string) and *secret* (string)
    - These can be entered manually, or simply copied from a json file generated with [authy-scripts/authyparse.py/filter_fields()](https://gitlab.com/rithvikvibhu/authy-scripts). This lets you save more info, which can be used in the future.

- **specials**
    - Simple key-value pairs for storing various pieces of text.
    - Escape characters like \n are allowed here.
    - Can be used for random phrases, paperkeys, etc.

## How to use

- Create a json file based on **sample_secrets.json**
- Change JSON_FILENAME to the filename of your secret file.
- Run `python psb.py`